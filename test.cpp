//#include "NvInfer.h"
//#include "cuda_runtime_api.h"
//#include <fstream>
//#include <iostream>
//#include <map>
//#include <vector>
//#include <chrono>
//#include <cmath>
//#include <cassert>
//#include <fmt/core.h>
//#include <memory>
//#include "include/logging.h"
//#include <opencv2/opencv.hpp>
//#define CHECK(status) \
//    do\
//    {\
//        auto ret = (status);\
//        if (ret != 0)\
//        {\
//            std::cerr << "Cuda failure: " << ret << std::endl;\
//            abort();\
//        }\
//    } while (0)
//
//
//static const int INPUT_H = 80;
//static const int INPUT_W = 80;
//static const int OUTPUT_SIZE = 3;
//
//const char *INPUT_BLOB_NAME = "data";
//const char *OUTPUT_BLOB_NAME = "prob";
//
//using namespace nvinfer1;
//static Logger gLogger(Severity::kVERBOSE);
//
//
//std::string format_layer_name(std::string lname, std::string post_fix) {
//    return fmt::format("{}.{}", lname, post_fix);
//}
//
//std::map<std::string, Weights> loadWeights(const std::string file) {
//    std::cout << "Loading weights: " << file << std::endl;
//    std::map<std::string, Weights> WeightMap; //init WeightMap
//
//    std::ifstream input(file);
//    assert(input.is_open() && "Unable to load weight file.");
//
//    int32_t count;
//    input >> count;
//    assert(count > 0 && "Invalid weight map file");
//
//    while (count--) {
//        Weights wt{DataType::kFLOAT, nullptr, 0};
//        uint32_t size;
//        std::string name;
//        input >> name >> std::dec >> size;
//        wt.type = DataType::kFLOAT;
//
////        std::vector<uint32_t> val(size);
//        uint32_t* val = reinterpret_cast<uint32_t*>(malloc(sizeof(val) * size));
//        for (uint32_t x = 0, y = size; x < y; ++x) {
//            input >> std::hex >> val[x];
//        }
//
//        wt.values = val;
//        wt.count = size;
//        WeightMap[name] = wt;
//    }
//    return WeightMap;
//
//}
//
//
//IScaleLayer *addBatchNorm2d(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                            std::string lname, float eps) {
//    std::cout << lname << std::endl;
//    float *gamma = (float *) WeightMap[format_layer_name(lname, "weight")].values;
//    float *beta = (float *) WeightMap[format_layer_name(lname, "bias")].values;
//    float *mean = (float *) WeightMap[format_layer_name(lname, "running_mean")].values;
//    float *var = (float *) WeightMap[format_layer_name(lname, "running_var")].values;
//    int len = WeightMap[format_layer_name(lname, "running_var")].count;
//    std::cout << "len :" << len << std::endl;
//
//    //scale
////    std::vector<float> scval(len);
//    float *scval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
//    for (int i = 0; i < len; i++) {
//        scval[i] = gamma[i] / std::sqrt(var[i] + eps);
//    }
//    Weights scale{DataType::kFLOAT, scval, len};
//
//    // shift
////    std::vector<float> shval(len);
//    float *shval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
//    for (int i = 0; i < len; i++) {
//        shval[i] = beta[i] - mean[i] / std::sqrt(var[i] + eps);
//    }
//    Weights shift{DataType::kFLOAT, shval, len};
//
//    //power
////    std::vector<float> pval(len);
//    float *pval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
//    for (int i = 0; i < len; i++) {
//        pval[i] = 1.0;
//    }
//    Weights power{DataType::kFLOAT, pval, len};
//
//    WeightMap[format_layer_name(lname, "scale")] = scale;
//    WeightMap[format_layer_name(lname, "shift")] = shift;
//    WeightMap[format_layer_name(lname, "power")] = power;
//    IScaleLayer *scale_1 = network->addScale(input, ScaleMode::kCHANNEL, shift, scale, power);
//    assert(scale_1);
//
//    return scale_1;
//}
//
//
//IElementWiseLayer *addPrelu(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                            std::string lname) {
//    std::cout << lname << std::endl;
//    int len = WeightMap[format_layer_name(lname, "weight")].count;
//
//    // shift
////    std::vector<float> sh(len);
//    float *sh = reinterpret_cast<float*>(malloc(sizeof(float)*len));
//    for (int i = 0; i < len; i++) {
//        sh[i] = 0.0;
//    }
//    Weights shift{DataType::kFLOAT, sh, len};
//
//    //scale
//    float *weight = (float *) WeightMap[format_layer_name(lname, "weight")].values;
////    std::vector<float> sc(len);
//    float *sc = reinterpret_cast<float*>(malloc(sizeof(float)*len));
//    for (int i = 0; i < len; i++) {
//        sc[i] = -weight[i];
//    }
//    Weights scale{DataType::kFLOAT, sc, len};
//
//    //power
////    std::vector<float> p(len);
//    float *p  = reinterpret_cast<float*>(malloc(sizeof(float)*len));
//    for (int i = 0; i < len; i++) {
//        p[i] = 1.0;
//    }
//    Weights power{DataType::kFLOAT, p, len};
//
//    //-1 scale
//    float *s = reinterpret_cast<float*>(malloc(sizeof(float)*len));
//    for (int i = 0; i < len; i++) {
//        s[i] = -1.0;
//    }
//    Weights sign{DataType::kFLOAT, s, len};
//
//    /* add WeightMap */
//
//    IScaleLayer *ivsprl = network->addScale(input, ScaleMode::kCHANNEL, shift, scale, power);
//    assert(ivsprl);
//
//    IActivationLayer *relu1 = network->addActivation(*ivsprl->getOutput(0), ActivationType::kRELU);
//    assert(relu1);
//
//    IScaleLayer *ivspr2 = network->addScale(*relu1->getOutput(0), ScaleMode::kCHANNEL, shift, sign, power);
//    assert(ivspr2);
//
//    IActivationLayer *relu2 = network->addActivation(input, ActivationType::kRELU);
//    assert(relu2);
//
//    IElementWiseLayer *prelu = network->addElementWise(*ivspr2->getOutput(0), *relu2->getOutput(0),
//                                                       ElementWiseOperation::kSUM);
//    assert(prelu);
//
//    return prelu;
//}
//
//
//IPoolingLayer *addAdaptiveAvPooling(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                                    int32_t outChannel=1){
//    Dims dim = input.getDimensions();
//    //H stride
//    int H = dim.d[1];
//    int strideH = std::floor(H/outChannel);
//    int kernel_size_H = H - (outChannel - 1)*strideH;
//
//    int W = dim.d[2];
//    int strideW = std::floor(W/outChannel);
//    int kernel_size_W = W - (outChannel - 1)*strideW;
//    IPoolingLayer *poolAd = network->addPoolingNd(input, PoolingType::kAVERAGE, DimsHW{kernel_size_H, kernel_size_W});
//    poolAd->setPaddingNd(DimsHW{0, 0});
//    return poolAd;
//}
//
//
//IElementWiseLayer *addConvBlock(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                                std::string lname, int32_t outChannel, int32_t ksize, int32_t stride, int32_t padding,
//                                int32_t group) {
//    std::cout << lname << std::endl;
//    Weights emptywts{DataType::kFLOAT, nullptr, 0};
//    IConvolutionLayer *conv1 = network->addConvolutionNd(input, outChannel, DimsHW{ksize, ksize},
//                                                         WeightMap[format_layer_name(lname, "conv.weight")], emptywts);
//    assert(conv1);
//    conv1->setStrideNd(DimsHW{stride, stride});
//    conv1->setPaddingNd(DimsHW{padding, padding});
//    conv1->setNbGroups(group);
//
//    IScaleLayer *bn1 = addBatchNorm2d(network, WeightMap, *conv1->getOutput(0), format_layer_name(lname, "bn"), 1e-5);
//    assert(bn1);
//
//    IElementWiseLayer *prelu1 = addPrelu(network, WeightMap, *bn1->getOutput(0), format_layer_name(lname, "prelu"));
//    assert(prelu1);
//    return prelu1;
//}
//
//
//IScaleLayer *addLinearBlock(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                            std::string lname, int32_t outChannel, int32_t ksize = 1, int32_t stride = 1,
//                            int32_t padding = 0, int32_t group = 1) {
//    std::cout << lname << std::endl;
//    Weights emptywts{DataType::kFLOAT, nullptr, 0};
//    IConvolutionLayer *conv1 = network->addConvolutionNd(input, outChannel, DimsHW{ksize, ksize},
//                                                         WeightMap[format_layer_name(lname, "conv.weight")], emptywts);
//    assert(conv1);
//    conv1->setStrideNd(DimsHW{stride, stride});
//    conv1->setPaddingNd(DimsHW{padding, padding});
//    conv1->setNbGroups(group);
//
//    IScaleLayer *bn1 = addBatchNorm2d(network, WeightMap, *conv1->getOutput(0), format_layer_name(lname, "bn"), 1e-5);
//    assert(bn1);
//    return bn1;
//}
//
//
//
//ILayer *addDepthWise(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                     std::string lname, int32_t c1_out, int32_t c2_out, int32_t c3_out, int32_t ksize, int32_t stride,
//                     int32_t padding, int32_t group, bool use_res_conection=true){
//    std::cout << lname << std::endl;
//    Weights emptywts{DataType::kFLOAT, nullptr, 0};
//    IElementWiseLayer *conv = addConvBlock(network, WeightMap, input, format_layer_name(lname, "conv"), c1_out, 1, 1, 0, 1);
//    assert(conv);
//
//    IElementWiseLayer *conv_dw = addConvBlock(network, WeightMap, *conv->getOutput(0), format_layer_name(lname, "conv_dw"), c2_out, ksize, stride, padding, group);
//    assert(conv_dw);
//
//    IScaleLayer *project = addLinearBlock(network, WeightMap, *conv_dw->getOutput(0), format_layer_name(lname, "project"), c3_out);
//    assert(project);
//
//    if (!use_res_conection){
//        return project;
//    }
//    else {
//        IElementWiseLayer *DeptOut = network->addElementWise(input, *project->getOutput(0), ElementWiseOperation::kSUM);
//        assert(DeptOut);
//        return DeptOut;
//    }
//}
//
//
//ILayer * addDeptWiseSE(INetworkDefinition *network, std::map<std::string, Weights> &WeightMap, ITensor &input,
//                       std::string lname, int32_t c1_out, int32_t c2_out, int32_t c3_out, int32_t ksize, int32_t stride,
//                       int32_t padding, int32_t group){
//    std::cout << lname << std::endl;
//    Weights emptywts{DataType::kFLOAT, nullptr, 0};
//
//    IElementWiseLayer *conv = addConvBlock(network, WeightMap, input, format_layer_name(lname, "conv"), c1_out, 1, 1, 0, 1);
//    assert(conv);
//
//    IElementWiseLayer *conv_dw = addConvBlock(network, WeightMap, *conv->getOutput(0), format_layer_name(lname, "conv_dw"), c2_out, ksize, stride, padding, group);
//    assert(conv_dw);
//
//    IScaleLayer *project = addLinearBlock(network, WeightMap, *conv_dw->getOutput(0), format_layer_name(lname, "project"), c3_out);
//    assert(project);
//
//    // SE Model
//    int outChannel1 = WeightMap[format_layer_name(lname, "se_module.bn1.running_var")].count;
//    int outChannel2 = WeightMap[format_layer_name(lname, "se_module.bn2.running_var")].count;
//
//    IPoolingLayer *adaptivePooling = addAdaptiveAvPooling(network, WeightMap, *project->getOutput(0));
//    assert(adaptivePooling);
//
//    IConvolutionLayer *fc1 = network->addConvolutionNd(*adaptivePooling->getOutput(0), outChannel1,
//                                                       DimsHW{1, 1}, WeightMap[format_layer_name(lname, "se_module.fc1.weight")],
//                                                       emptywts);
//    assert(fc1);
//    fc1->setStrideNd(DimsHW{1, 1});
//    fc1->setPaddingNd(DimsHW{0, 0});
//
//    IScaleLayer *bn1 = addBatchNorm2d(network, WeightMap, *fc1->getOutput(0), format_layer_name(lname, "se_module.bn1"), 1e-5);
//    assert(bn1);
//
//    IActivationLayer *relu = network->addActivation(*bn1->getOutput(0), ActivationType::kRELU);
//    assert(relu);
//
//    IConvolutionLayer *fc2 = network->addConvolutionNd(*relu->getOutput(0), outChannel2,
//                                                       DimsHW{1, 1},
//                                                       WeightMap[format_layer_name(lname, "se_module.fc2.weight")], emptywts);
//    assert(fc2);
//    fc2->setStrideNd(DimsHW{1, 1});
//    fc2->setPaddingNd(DimsHW{0, 0});
//
//    IScaleLayer *bn2 = addBatchNorm2d(network, WeightMap, *fc2->getOutput(0), format_layer_name(lname, "se_module.bn2"), 1e-5);
//    assert(bn2);
//
//    IActivationLayer *sig = network->addActivation(*bn2->getOutput(0), ActivationType::kSIGMOID);
//    assert(sig);
//
//    IElementWiseLayer *mul = network->addElementWise(*project->getOutput(0), *sig->getOutput(0), ElementWiseOperation::kPROD);
//    assert(mul);
//
//    IElementWiseLayer *outSE = network->addElementWise(input, *mul->getOutput(0), ElementWiseOperation::kSUM);
//    assert(outSE);
//
//    return outSE;
//}
//
//
////create the engine
//IHostMemory* createEngine(unsigned int maxBatchSize, IBuilder *builder, IBuilderConfig *config, DataType dt){
////    uint32_t flag = 1U << static_cast<uint32_t>(NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);
//    std::unique_ptr<INetworkDefinition> network(builder->createNetworkV2(0U));
//
//    ITensor *data = network->addInput(INPUT_BLOB_NAME, dt, Dims3{3, INPUT_H, INPUT_W});
//    assert(data);
//
//    std::map<std::string, Weights> WeightMap = loadWeights("../face_spoof.wts");
//
//    Weights emptywts{DataType::kFLOAT, nullptr, 0};
//
////    IConvolutionLayer *output = network->addConvolutionNd(*data, 32, DimsHW{3, 3}, WeightMap["conv1.conv.weight"], emptywts);
////    assert(output);
////
////    output->setStrideNd(DimsHW{2, 2});
////    output->setPaddingNd(DimsHW{1, 1});
////    output->setNbGroups(1);IScaleLayer *bn1 = addBatchNorm2d(network.get(), WeightMap, *output->getOutput(0), "conv1.bn", 1e-5);
////    IElementWiseLayer *prelu1 = addPrelu(network.get(), WeightMap, *bn1->getOutput(0), "conv1.prelu");
//
//  // convBlock1
//    auto *out = addConvBlock(network.get(), WeightMap, *data, "conv1", 32, 3, 2, 1, 1);
//    // convBlock2
//    out = addConvBlock(network.get(), WeightMap, *out->getOutput(0), "conv2_dw", 32, 3, 1, 1, 32);
//    //DeptWise1
//    auto *out1 = addDepthWise(network.get(), WeightMap, *out->getOutput(0), "conv_23", 103, 103, 64, 3, 2, 1, 103, false);
//    //ResidualSE
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_3.model.0", 13, 13, 64, 3, 1, 1, 13);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_3.model.1", 13, 13, 64, 3, 1, 1, 13);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_3.model.2", 13, 13, 64, 3, 1, 1, 13);
//    out1 = addDeptWiseSE(network.get(), WeightMap, *out1->getOutput(0), "conv_3.model.3", 13, 13, 64, 3, 1, 1, 13);
//    //DeptWise2
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_34", 231, 231, 128, 3, 2, 1, 231, false);
//    //Residual2
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.0", 231, 231, 128, 3, 1, 1, 231);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.1", 52, 52, 128, 3, 1, 1, 52);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.2", 26, 26, 128, 3, 1, 1, 26);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.3", 77, 77, 128, 3, 1, 1, 77);
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.4", 26, 26, 128, 3, 1, 1, 26);
//    out1 = addDeptWiseSE(network.get(), WeightMap, *out1->getOutput(0), "conv_4.model.5", 26, 26, 128, 3, 1, 1, 26);
//    //DeptWise3
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_45", 308, 308, 128, 3, 2, 1, 308, false);
//    //Residual3
//    out1 = addDepthWise(network.get(), WeightMap, *out1->getOutput(0), "conv_5.model.0", 26, 26, 128, 3, 1, 1, 26);
//    out1 = addDeptWiseSE(network.get(), WeightMap, *out1->getOutput(0), "conv_5.model.1", 26, 26, 128, 3, 1, 1, 26);
//    //convBlock2
//    auto *out2 = addConvBlock(network.get(), WeightMap, *out1->getOutput(0), "conv_6_sep", 512, 1, 1, 0, 1);
//    //LinearBlock1
//    auto *out3 = addLinearBlock(network.get(), WeightMap, *out2->getOutput(0), "conv_6_dw", 512, 5, 1, 0, 512);
//    //flatten
//    IFullyConnectedLayer *linear = network.get()->addFullyConnected(*out3->getOutput(0), 128, WeightMap["linear.weight"], emptywts);
//    //bn1d
//    auto *out4 = addBatchNorm2d(network.get(), WeightMap, *linear->getOutput(0), "bn", 1e-5);
//    //prob
//    IFullyConnectedLayer *output = network->addFullyConnected(*out4->getOutput(0), 3, WeightMap["prob.weight"], emptywts);
//    output->getOutput(0)->setName(OUTPUT_BLOB_NAME);
//    std::cout << "set name output" << std::endl;
//    network->markOutput(*output->getOutput(0));
//
//
//    //build engine
//    builder->setMaxBatchSize(maxBatchSize);
//    config->setMemoryPoolLimit(MemoryPoolType::kWORKSPACE, 1U<<30);
//
//    IHostMemory *serializedModel = builder->buildSerializedNetwork(*network, *config);
//    std::cout << "build out" << std::endl;
//
//
//    return serializedModel;
//}
//
//
//void APIToModel(unsigned int maxBatchSize, float *input, float *output){
//    std::unique_ptr<IBuilder> builder(createInferBuilder(gLogger));
//    std::unique_ptr<IBuilderConfig> config(builder->createBuilderConfig());
//    std::unique_ptr<IHostMemory> serializedModel(createEngine(maxBatchSize, builder.get(), config.get(), DataType::kFLOAT));
//
//    std::unique_ptr<IRuntime> runtime(createInferRuntime(gLogger));
//    assert(runtime != nullptr);
//
//    std::unique_ptr<ICudaEngine> engine(runtime->deserializeCudaEngine(serializedModel->data(), serializedModel->size()));
//    assert (engine != nullptr);
//
//
//    // Run inference
//    std::unique_ptr<IExecutionContext> context(engine->createExecutionContext());
////    const ICudaEngine &engineBuild = context->getEngine();
//
//    assert(engine.get()->getNbBindings()==2);
//    void* buffers[2];
//
//    const int inputIndex = engine->getBindingIndex(INPUT_BLOB_NAME);
//    const int outputIndex = engine->getBindingIndex(OUTPUT_BLOB_NAME);
//
//    //Create GPU buffer on device
//    //input batchSize x 3 x H x W x sizeof(float)
//    //output batchSize x OUTPUT_SIZE * sizeof(float)
//    int batchSize =1;
//    CHECK(cudaMalloc(&buffers[inputIndex], batchSize * 3 * INPUT_H * INPUT_W * sizeof(float)));
//    CHECK(cudaMalloc(&buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float)));
//
//    //Create stream;
//    cudaStream_t stream;
//    CHECK(cudaStreamCreate(&stream));
//
//    // DMA input to device and DMA output to host
//    CHECK(cudaMemcpyAsync(buffers[inputIndex], input, batchSize * 3 * INPUT_H * INPUT_W * sizeof(float), cudaMemcpyHostToDevice, stream));
//    context.get()->enqueue(batchSize, buffers, stream, nullptr);
//    CHECK(cudaMemcpyAsync(output, buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float), cudaMemcpyDeviceToHost, stream));
//    cudaStreamSynchronize(stream);
//
//    //Release stream
//    cudaStreamDestroy(stream);
//    CHECK(cudaFree(buffers[inputIndex]));
//    CHECK(cudaFree(buffers[outputIndex]));
//
//
//
////    assert(serializedModel != nullptr);
////
////    std::ofstream file("../face_spoof.engine", std::ios::binary);
////    if (!file){
////        std::cout << "Could not open output file" << std::endl;
////    }
////    if (file){
////        file.write(reinterpret_cast<const char*>(serializedModel->data()), serializedModel->size());
////        file.close();
////    }
//}
//
//
//void doInference(float *input, float *output, int batchSize, std::string filename){
//    //read file
//    std::vector<char> deserializedModel;
//    size_t size{0};
//
//    std::ifstream file(filename, std::ios::binary);
//    if (file.good()){
//        file.seekg(0, file.end);
//        size = file.tellg();
//        file.seekg(0, file.beg);
//        deserializedModel = std::vector<char>(size);
//        assert(deserializedModel);
//        file.read(deserializedModel.data(), size);
//        file.close();
//    }
//    for (auto &i : deserializedModel){
//        std::cout << i << std::endl;
//    }
//
//    // deserialize
//    std::unique_ptr<IRuntime> runtime(createInferRuntime(gLogger));
//    assert(runtime != nullptr);
//
//    std::unique_ptr<ICudaEngine> engine(runtime->deserializeCudaEngine(deserializedModel.data(), size));
//    assert (engine != nullptr);
//
//
//    // Run inference
//    std::unique_ptr<IExecutionContext> context(engine->createExecutionContext());
//    const ICudaEngine &engineBuild = context->getEngine();
//
//    assert(engine.get()->getNbBindings()==2);
//    void* buffers[2];
//
//    const int inputIndex = engine.get()->getBindingIndex(INPUT_BLOB_NAME);
//    const int outputIndex = engine.get()->getBindingIndex(OUTPUT_BLOB_NAME);
//
//    //Create GPU buffer on device
//    //input batchSize x 3 x H x W x sizeof(float)
//    //output batchSize x OUTPUT_SIZE * sizeof(float)
//    CHECK(cudaMalloc(&buffers[inputIndex], batchSize * 3 * INPUT_H * INPUT_W * sizeof(float)));
//    CHECK(cudaMalloc(&buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float)));
//
//    //Create stream;
//    cudaStream_t stream;
//    CHECK(cudaStreamCreate(&stream));
//
//    // DMA input to device and DMA output to host
//    CHECK(cudaMemcpyAsync(buffers[inputIndex], input, batchSize * 3 * INPUT_H * INPUT_W * sizeof(float), cudaMemcpyHostToDevice, stream));
//    context->enqueue(batchSize, buffers, stream, nullptr);
//    CHECK(cudaMemcpyAsync(output, buffers[outputIndex], batchSize * OUTPUT_SIZE * sizeof(float), cudaMemcpyDeviceToHost, stream));
//    cudaStreamSynchronize(stream);
//
//    //Release stream
//    cudaStreamDestroy(stream);
//    CHECK(cudaFree(buffers[inputIndex]));
//    CHECK(cudaFree(buffers[outputIndex]));
//
//}
//int main() {
////    APIToModel(1);
//
//    //create input
////    cv::Mat img = cv::imread("../1.png");
////    cv::Mat imgresize;
////    cv::resize(img, imgresize, cv::Size{80, 80});
////    imgresize = cv::dnn::blobFromImage(imgresize);
////    static std::vector<float> data(3 * INPUT_H * INPUT_W);
////    data.assign(imgresize.begin<float>(), imgresize.end<float>());
//    static std::vector<float> input(3 * INPUT_H * INPUT_W);
//    for (int i = 0; i < 3 * INPUT_H * INPUT_W; i++){
//        input[i] = 2.0;
//    }
//    static std::vector<float> prob(OUTPUT_SIZE);
//
//    auto start = std::chrono::system_clock::now();
//    APIToModel(1, input.data(), prob.data());
//    auto end = std::chrono::system_clock::now();
//    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
//
//    std::cout << "\nOutput:\n\n";
//    for (unsigned int i = 0; i < OUTPUT_SIZE; i++){
//        std::cout << prob[i] << ", ";
////        if (i % 10 == 0) std::cout << 1 / 10 << std::endl;
//    }
//    std::cout <<std::endl;
//    return 0;
//}
